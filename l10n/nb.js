OC.L10N.register(
    "integration_slack",
    {
    "Error getting OAuth access token. " : "Feil ved henting av OAuth-tilgangstoken. ",
    "Error during OAuth exchanges" : "En feil oppsto under OAuth-utvekslingen",
    "Bad HTTP method" : "HTTP-metode er feil",
    "Bad credentials" : "Påloggingsdetaljene er feil",
    "OAuth access token refused" : "OAuth access token ble avslått",
    "Connected accounts" : "Tilknyttede kontoer",
    "Client ID" : "Klient-ID",
    "Application secret" : "Søknadshemmelighet",
    "Use a popup to authenticate" : "Bruk en popup for å autentisere",
    "Invalid access token" : "Ugyldig tilgangstoken",
    "Files" : "Filer",
    "Channel" : "Kanal",
    "Set expiration date" : "Sett utløpsdato",
    "Comment" : "Kommentar",
    "Cancel" : "Avbryt",
    "View only" : "Kun se",
    "Edit" : "Rediger"
},
"nplurals=2; plural=(n != 1);");
