OC.L10N.register(
    "integration_slack",
    {
    "Error getting OAuth access token. " : "שגיאה בהשגת אסימון גישה ל-OAuth.",
    "Error during OAuth exchanges" : "שגיאה במהלך החלפות OAuth",
    "Bad HTTP method" : "שגיאה במתודת HTTP",
    "Bad credentials" : "פרטי גישה שגויים",
    "OAuth access token refused" : "אסימון הגישה ב־OAuth סורב",
    "Connected accounts" : "חשבונות מקושרים",
    "Client ID" : "מזהה לקו",
    "Files" : "קבצים",
    "Channel" : "ערוץ",
    "Type" : "סוג",
    "Set expiration date" : "הגדרת תאריך תפוגה",
    "Comment" : "הערה",
    "Cancel" : "ביטול",
    "View only" : "לצפיה בלבד",
    "Edit" : "עריכה",
    "Upload files" : "העלאת קבצים"
},
"nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;");
