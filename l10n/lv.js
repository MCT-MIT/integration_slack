OC.L10N.register(
    "integration_slack",
    {
    "Client ID" : "Klienta ID",
    "Files" : "Faili",
    "Type" : "Tips",
    "Set expiration date" : "Uzstādīt beigu termiņu",
    "Comment" : "Komentārs",
    "Edit" : "Rediģēt"
},
"nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);");
