OC.L10N.register(
    "integration_slack",
    {
    "Error getting OAuth access token. " : "Błąd podczas pobierania tokena dostępu OAuth.",
    "Error during OAuth exchanges" : "Błąd podczas zamiany OAuth",
    "Bad HTTP method" : "Zła metoda HTTP",
    "Bad credentials" : "Złe poświadczenia",
    "OAuth access token refused" : "Odmowa tokena dostępu OAuth",
    "Connected accounts" : "Połączone konta",
    "Client ID" : "ID klienta",
    "Application secret" : "Tajny klucz aplikacji",
    "Use a popup to authenticate" : "Użyj wyskakującego okienka do uwierzytelnienia",
    "Invalid access token" : "Nieprawidłowy token dostępu",
    "Files" : "Pliki",
    "Channel" : "Kanał",
    "Type" : "Rodzaj",
    "Set expiration date" : "Ustaw datę wygaśnięcia",
    "Comment" : "Komentarz",
    "Cancel" : "Anuluj",
    "View only" : "Tylko podgląd",
    "Edit" : "Edycja",
    "Upload files" : "Wyślij pliki"
},
"nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);");
