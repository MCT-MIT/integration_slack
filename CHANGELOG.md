# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## 1.0.1 - 2023-08-22
### Added
* redirect uri in oauth requests
### Fixed
* slack profile pictures url fix

## 1.0.0 – 2023-07-20
### Added
* the app
